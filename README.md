# tictactoe
# an adventure in a google form

## Introduction
        
	Google Forms have provided the capability to add Multiple Choice Questions.
	Further, depending on the answer, you can be led to further sections
	Thus, How capable can you make Google Forms?
	Can it be a game?
		Random: No (I guess?)
		Deterministic: Shouldn't It?
	
## Objective

	Run a tictactoe game using Google Forms

## Progress

	2019/11/11: 
		Basic layout confirmed. 
		As idiotic as it might sound, will be using screenshots of pictures as options
		( 'cause, for now, I just need this thing up and running!)
	2019/11/12:
		Will be using xkcd tictactoe as layout.
		Link: https://xkcd.com/832_large
